﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

namespace InsanePluginLibrary.TransformUtil
{

	/// <summary>
	/// This class contains methods for aligning objects with the terrain
	/// </summary>
	static public class TerrainAssetAlignTools
	{
		/// <summary>
		/// /// The actions the alignment tool can execute
		/// </summary>
		public enum ActionType
		{
			/// <summary>
			/// Position alignment using the transform as single factor
			/// </summary>
			AlignTransform,
			/// <summary>
			/// Position alignment using the bounding box as factor
			/// </summary>
			AlignBoundingBox,
			/// <summary>
			/// Rotation aligntment.
			/// </summary>
			AlignNormal
		}

		/// <summary>
		/// The align method when using AABB
		/// </summary>
		public enum AABBMethod
		{
			/// <summary>
			/// Aligns with the bounding box bottom
			/// </summary>
			Bottom,
			/// <summary>
			/// Aligns with the bounding box middle point
			/// </summary>
			Middle,
			/// <summary>
			/// Aligns with the bounding box top
			/// </summary>
			Top
		}



		#region Menu

		/// <summary>
		/// Snaps the transform center to the terrain below it
		/// </summary>
		[MenuItem("Align/Transform/Position %&LEFT", false, 20)]
		static public void SnapTransforms()
		{
			SnapTransforms(ActionType.AlignTransform, AABBMethod.Middle);
		}

		/// <summary>
		/// Snaps the transform to the terrain below aligning it to the top of the bounding box
		/// </summary>
		[MenuItem("Align/Bounding Box/Top %&UP", false, 20)]
		static public void SnapAabbTop()
		{
			SnapTransforms(ActionType.AlignBoundingBox, AABBMethod.Top);
		}

		/// <summary>
		/// Snaps the transform to the terrain below aligning it to the middle of the bounding box
		/// </summary>
		[MenuItem("Align/Bounding Box/Middle %&RIGHT", false, 20)]
		static public void SnapAabbMiddle()
		{
			SnapTransforms(ActionType.AlignBoundingBox, AABBMethod.Middle);
		}

		/// <summary>
		/// Snaps the transform to the terrain below aligning it to the bottom of the bounding box
		/// </summary>
		[MenuItem("Align/Bounding Box/Bottom %&DOWN", false, 20)]
		static public void SnapAabbBottom()
		{
			SnapTransforms(ActionType.AlignBoundingBox, AABBMethod.Bottom);
		}

		/// <summary>
		/// Aligns the transform with the terrain normal below it.
		/// </summary>
		[MenuItem("Align/Normals/Align With Terrain %&0", false, 20)]
		static public void AlignNormals()
		{
			SnapTransforms(ActionType.AlignNormal, AABBMethod.Bottom);
		}

		#endregion



		/// <summary>
		/// The generic input method for adjusting transforms
		/// </summary>
		/// <param name="action">The type of action</param>
		/// <param name="aabbMethod">The AABB parameter if aplicable</param>
		static public void SnapTransforms(ActionType action, AABBMethod aabbMethod)
		{
			GameObject[] objs = Selection.gameObjects;

			if (objs.Length == 0)
			{
				Debug.Log("No object selected");
				return;
			}

			if (objs.Length == 1)
			{
				if (objs[0].transform.childCount > 0)
				{
					Debug.Log("Snapping Child Objects");
					ExecuteChildren(objs[0].transform, action, aabbMethod);
				}
				else
				{
					Debug.Log("Snapping Single Object");
					ExecuteSingle(objs[0].transform, action, aabbMethod);
				}
				return;
			}

			if (objs.Length >= 2)
			{
				var rootObj = Selection.activeGameObject.transform.root.gameObject;
				if (objs.Contains(rootObj))
				{
					Debug.Log("Snapping Child Objects");
					ExecuteChildren(objs[0].transform, action, aabbMethod);
				}
				else
				{
					Debug.Log("Snapping All Objects");
					Transform[] transforms = objs.Select(item => item.transform).ToArray();
					ExecuteList(transforms, action, aabbMethod);
				}

				return;
			}
		}



		#region Execution

		/// <summary>
		/// Executes actions on a list of Transforms
		/// </summary>
		/// <param name="objList">The list to be executed</param>
		/// <param name="actionType">The action type</param>
		/// <param name="aabbMethod">The AABB parameter if aplicable</param>
		static public void ExecuteList(Transform[] objList, ActionType actionType, AABBMethod aabbMethod)
		{
			switch (actionType)
			{
				case ActionType.AlignTransform:

					foreach (var transform in objList)
					{
						TransformSnap(transform);
					}

					break;
				case ActionType.AlignBoundingBox:

					foreach (var transform in objList)
					{
						AabbSnap(transform, aabbMethod);
					}

					break;

				case ActionType.AlignNormal:

					foreach (var transform in objList)
					{
						SnapNormal(transform);
					}

					break;
			}
		}

		/// <summary>
		/// Executes actions in the children of this transform
		/// </summary>
		/// <param name="obj">The object which will have its values updated</param>
		/// <param name="actionType">The action type</param>
		/// <param name="aabbMethod">The AABB parameter if aplicable</param>
		static public void ExecuteChildren(Transform obj, ActionType actionType, AABBMethod aabbMethod)
		{
			switch (actionType)
			{
				case ActionType.AlignTransform:

					for (int i = 0; i < obj.childCount; i++)
					{
						TransformSnap(obj.GetChild(i));
					}

					break;

				case ActionType.AlignBoundingBox:

					for (int i = 0; i < obj.childCount; i++)
					{
						AabbSnap(obj.GetChild(i), aabbMethod);
					}

					break;

				case ActionType.AlignNormal:

					for (int i = 0; i < obj.childCount; i++)
					{
						SnapNormal(obj.GetChild(i));
					}

					break;
			}
		}

		/// <summary>
		/// Executes actions on a single transform
		/// </summary>
		/// <param name="obj">The object which will have its values updated</param>
		/// <param name="actionType">The action type</param>
		/// <param name="aabbMethod">The AABB parameter if aplicable</param>
		static public void ExecuteSingle(Transform obj, ActionType actionType, AABBMethod aabbMethod)
		{
			switch (actionType)
			{
				case ActionType.AlignTransform:

					TransformSnap(obj);

					break;

				case ActionType.AlignBoundingBox:

					AabbSnap(obj, aabbMethod);

					break;

				case ActionType.AlignNormal:

					SnapNormal(obj);

					break;
			}
		}

		#endregion



		#region Transform

		/// <summary>
		/// Snaps the transform to the terrain imediately below it
		/// </summary>
		/// <param name="obj">The object which will have its values updated</param>
		static public void TransformSnap(Transform obj)
		{
			Vector3 snapPosition;
			if (GetHeight(obj.position, out snapPosition))
			{
				obj.position = snapPosition;
			}
		}

		#endregion



		#region AABB

		/// <summary>
		/// Snaps the transform to the terrain imediately below it, using the mesh information to fine tune the alignment
		/// </summary>
		/// <param name="obj">The object which will have its values updated</param>
		/// <param name="method">The AABB parameter if applicable</param>
		static public void AabbSnap(Transform obj, AABBMethod method)
		{
			var boundingBox = InsanePluginLibrary.Mesh.MeshUtils.GetAABB(obj.gameObject);
			Vector3 targetPosition;
			float heightOffset = 0;
			GetHeight(obj.position, out targetPosition);
			switch (method)
			{
				case AABBMethod.Bottom:
					heightOffset = boundingBox.Size.y * 0.5f;
					obj.position = targetPosition + new Vector3(0, heightOffset, 0);
					break;
				case AABBMethod.Middle:
					obj.position = targetPosition;
					break;
				case AABBMethod.Top:
					heightOffset = boundingBox.Size.y * 0.5f;
					obj.position = targetPosition - new Vector3(0, heightOffset, 0);
					break;
				default:
					break;
			}
		}

		#endregion



		#region Normal

		/// <summary>
		/// Aligns the transform rotation to the terrain below
		/// </summary>
		/// <param name="obj">The transform to be aligned</param>
		static public void SnapNormal(Transform obj)
		{
			Vector3 snapPosition;
			if (GetNormal(obj.position, out snapPosition))
			{
				obj.up = snapPosition;
			}
		}

		#endregion



		#region Util

		/// <summary>
		/// Gets the height of the terrain at the X and Z coordinate of this position
		/// </summary>
		/// <param name="position">The position of the object to be aligned</param>
		/// <param name="newPosition">The new position after positive raycast</param>
		/// <returns>True if the raycast is successful</returns>
		static public bool GetHeight(Vector3 position, out Vector3 newPosition)
		{
			bool returnValue;
			var ray = new Ray(new Vector3(position.x, position.y + 500, position.z), Vector3.down);

			RaycastHit hit;
			if (returnValue = Physics.Raycast(ray, out hit, 1000, LayerMask.GetMask("Terrain")))
			{
				newPosition = hit.point;
			}
			else
			{
				newPosition = position;
			}
			return returnValue;
		}

		/// <summary>
		/// Gets the normal of the terrain at the X and Z coordinate of this position
		/// </summary>
		/// <param name="position">The position of the object to be aligned</param>
		/// <param name="normal">The normal after positive raycast</param>
		/// <returns>True if the raycast is successful</returns>
		static public bool GetNormal(Vector3 position, out Vector3 normal)
		{
			bool returnValue;
			var ray = new Ray(new Vector3(position.x, position.y + 500, position.z), Vector3.down);

			RaycastHit hit;
			if (returnValue = Physics.Raycast(ray, out hit, 1000, LayerMask.GetMask("Terrain")))
			{
				normal = hit.normal;
			}
			else
			{
				normal = position;
			}
			return returnValue;
		}

		#endregion
	}
}
