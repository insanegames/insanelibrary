﻿/*
	Copyright 2015 Danilo Nishimura

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

using UnityEditor;
using UnityEngine;

namespace InsaneEditorLibrary.Scriptable
{
	internal class ScriptableObjectMenu
	{
		// Adapted from https://gist.github.com/mstevenson/4726563
		[MenuItem("Assets/Create/Create ScriptableObject From Script", false, 10000)]
		public static void CreateAsset()
		{
			ScriptableObject asset = ScriptableObject.CreateInstance(Selection.activeObject.name);
			var path = AssetDatabase.GetAssetPath(Selection.activeObject.GetInstanceID());
			path = path.Substring(0, path.LastIndexOf('/'));
			AssetDatabase.CreateAsset(asset, string.Format("{0}/{1}.asset", path, Selection.activeObject.name));
			EditorGUIUtility.PingObject(asset);
		}
	}
}