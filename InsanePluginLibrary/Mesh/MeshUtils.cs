﻿/*
	Copyright 2015 Danilo Nishimura

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

using UnityEngine;

namespace InsanePluginLibrary.Mesh
{
	/// <summary>
	/// This class provides a simplistic view of a axis aligned bounding box
	/// </summary>
	public struct AABB
	{
		/// <summary>
		/// Most Negative Values
		/// </summary>
		public Vector3 Min;

		/// <summary>
		/// Most Positive Values
		/// </summary>
		public Vector3 Max;

		/// <summary>
		/// The difference between maximum and minimum values
		/// </summary>
		public Vector3 Size
		{
			get
			{
				return Max - Min;
			}
		}

		/// <summary>
		/// The middle point between max and min.
		/// </summary>
		public Vector3 Center
		{
			get
			{
				return (Max + Min) * 0.5f;
			}
		}
	}

	/// <summary>
	/// This class has some utilities for mesh handling.
	/// </summary>
	public static class MeshUtils
	{
		/// <summary>
		/// Calculates the world-space size of a mesh group.
		/// Remark: May not be up-to-date with vertex transformations in shader code.
		/// </summary>
		/// <param name="targetObject">The object to have its size calculated.</param>
		/// <returns>The Axis-Aligned Bounding box.</returns>
		public static AABB GetAABB(GameObject targetObject)
		{
			AABB boundingBox = new AABB();
			boundingBox.Min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
			boundingBox.Max = new Vector3(float.MinValue, float.MinValue, float.MinValue);

			//Pooling;
			UnityEngine.Mesh mesh;
			Vector3 vtxPt;
			MeshFilter filter;
			int filterCount, vtxCount;

			//Iterations;
			MeshFilter[] filters = targetObject.GetComponentsInChildren<MeshFilter>();
			filterCount = filters.Length;
			for (int i = 0; i < filterCount; i++)
			{
				filter = filters[i];
				mesh = filter.mesh;
				vtxCount = mesh.vertexCount;
				for (int j = 0; j < vtxCount; j++)
				{
					vtxPt = filter.transform.TransformPoint(mesh.vertices[j]);
					boundingBox.Min.x = Mathf.Min(boundingBox.Min.x, vtxPt.x);
					boundingBox.Min.y = Mathf.Min(boundingBox.Min.y, vtxPt.y);
					boundingBox.Min.z = Mathf.Min(boundingBox.Min.z, vtxPt.z);

					boundingBox.Max.x = Mathf.Max(boundingBox.Max.x, vtxPt.x);
					boundingBox.Max.y = Mathf.Max(boundingBox.Max.y, vtxPt.y);
					boundingBox.Max.z = Mathf.Max(boundingBox.Max.z, vtxPt.z);
				}
			}
			return boundingBox;
		}
	}
}