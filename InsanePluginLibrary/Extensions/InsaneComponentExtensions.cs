﻿/*
	Copyright 2017 Danilo Nishimura

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

using System;
using UnityEngine;

namespace InsanePluginLibrary.Extensions
{
	/// <summary>
	/// These extension methods helps make your code clearer by hiding some common practices behind nice methods.
	/// Methods may have two overloads, one for GameObject, other for Component, because both classes implement component methods but doesn't share a common base class.
	/// </summary>
	static public class InsaneComponentExtensions
	{
		#region GetOrAddComponent

		/// <summary>
		/// Performs a simple check and returns the desired behaviour.
		/// </summary>
		/// <typeparam name="T">The type to be checked and returned.</typeparam>
		/// <param name="gameObject">The game object to be checked.</param>
		/// <returns>The requested behaviour</returns>
		public static T GetOrAddComponent<T>(this GameObject gameObject) where T : MonoBehaviour
		{
			return gameObject.GetComponent<T>() ?? gameObject.AddComponent<T>();
		}

		/// <summary>
		/// Performs a simple check and returns the desired behaviour.
		/// </summary>
		/// <typeparam name="T">The type to be checked and returned.</typeparam>
		/// <param name="component">The game object to be checked.</param>
		/// <returns>The requested behaviour</returns>
		public static T GetOrAddComponent<T>(this Component component) where T : MonoBehaviour
		{
			return GetOrAddComponent<T>(component.gameObject);
		}

		#endregion GetOrAddComponent

		#region HasComponent

		/// <summary>
		/// Performs a null check against the requested behaviour.
		/// </summary>
		/// <typeparam name="T">The type to be checked.</typeparam>
		/// <param name="gameObject">The game object to be checked.</param>
		/// <returns>True if the component is found</returns>
		public static bool HasComponent<T>(this GameObject gameObject) where T : MonoBehaviour
		{
			return gameObject.GetComponent<T>() != null;
		}

		/// <summary>
		/// Performs a null check against the requested behaviour.
		/// </summary>
		/// <typeparam name="T">The type to be checked.</typeparam>
		/// <param name="component">The component to be checked.</param>
		/// <returns>True if the component is found</returns>
		public static bool HasComponent<T>(this Component component) where T : MonoBehaviour
		{
			return HasComponent<T>(component.gameObject);
		}

		#endregion HasComponent

		#region TryGet

		/// <summary>
		/// [Obsolete] Please use <see cref="TryGet{T}(GameObject, out T)"/>
		/// </summary>
		[System.Obsolete("Use TryGet<T>(GameObject gameObject, out T behaviour)")]
		public static bool HasComponent<T>(this GameObject gameObject, out T behaviour) where T : MonoBehaviour { return TryGet<T>(gameObject, out behaviour); }

		/// <summary>
		/// Performs a null check against the requested behaviour, also outputs the behaviour if found.
		/// </summary>
		/// <typeparam name="T">The type to be checked.</typeparam>
		/// <param name="gameObject">The game object to be checked.</param>
		/// <param name="behaviour"></param>
		/// <returns>True if the component is found</returns>
		public static bool TryGet<T>(this GameObject gameObject, out T behaviour) where T : MonoBehaviour
		{
			return (behaviour = gameObject.GetComponent<T>()) != null;
		}

		/// <summary>
		/// [Obsolete] Please use <see cref="TryGet{T}(GameObject, out T)"/>
		/// </summary>
		[System.Obsolete("Use TryGet<T>(Component component, out T behaviour)")]
		public static bool HasComponent<T>(this Component component, out T behaviour) where T : MonoBehaviour { return TryGet<T>(component, out behaviour); }

		/// <summary>
		/// Performs a null check against the requested behaviour, also outputs the behaviour if found.
		/// </summary>
		/// <typeparam name="T">The type to be checked.</typeparam>
		/// <param name="component">The component to be checked.</param>
		/// <param name="behaviour"></param>
		/// <returns>True if the component is found</returns>
		public static bool TryGet<T>(this Component component, out T behaviour) where T : MonoBehaviour
		{
			return (behaviour = component.gameObject.GetComponent<T>()) != null;
		}

		#endregion TryGet

		#region IfHasComponent

		/// <summary>
		/// Performs a null check against the requested behaviour.
		/// If the component exists, invoke the callback method passing the behaviour as argument.
		/// </summary>
		/// <typeparam name="T">The type to be checked.</typeparam>
		/// <param name="obj">The game object to be checked.</param>
		/// <param name="callbackMethod">The method or delegate to be invoked.</param>
		/// <returns>True if the component is found</returns>
		public static bool IfHasComponent<T>(this GameObject obj, Action<T> callbackMethod) where T : MonoBehaviour
		{
			T _component = obj.GetComponent<T>();
			if (_component) { callbackMethod(_component); }
			return _component != null;
		}

		/// <summary>
		/// Performs a null check against the requested behaviour.
		/// If the component exists, invoke the callback method passing the behaviour as argument.
		/// </summary>
		/// <typeparam name="T">The type to be checked.</typeparam>
		/// <param name="component">The component to be checked.</param>
		/// <param name="callbackMethod">The method or delegate to be invoked.</param>
		/// <returns>True if the component is found</returns>
		public static bool IfHasComponent<T>(this Component component, Action<T> callbackMethod) where T : MonoBehaviour
		{
			return IfHasComponent<T>(component.gameObject, callbackMethod);
		}

		#endregion IfHasComponent
	}
}