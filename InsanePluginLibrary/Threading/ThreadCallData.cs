﻿/*
	Copyright 2015 Danilo Nishimura

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

using System;

namespace InsanePluginLibrary.Threading
{
	/// <summary>
	/// This class holds information for the MainThread to queue and execute the methods.
	/// </summary>
	public class ThreadCallData : IDisposable
	{
		/// <summary>
		/// The wrapper for the method to be invoked by the MainThreadHelper
		/// </summary>
		/// <param name="sender"></param>
		public delegate void ThreadDataCallback(ThreadCallData sender);

		/// <summary>
		/// The name of the method to be Invoked.
		/// </summary>
		public string MethodName { get; private set; }

		/// <summary>
		/// Event triggered when the instance is disposed.
		/// </summary>
		internal event ThreadDataCallback OnDisposed = delegate { };

		/// <summary>
		/// Gets the method to be called.
		/// </summary>
		/// <value>The method.</value>
		public Action Method { get; private set; }

		/// <summary>
		/// Forces invokation to be try-catched.
		/// </summary>
		public bool ForceSafeMode { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="ThreadCallData"/> class.
		/// </summary>
		/// <param name="pMethod">P method.</param>
		public ThreadCallData(Action pMethod)
		{
			MethodName = pMethod.Method.Name;
			Method = pMethod;
		}

		/// <summary>
		/// Cancels the threaded call. Useful for delayed calls.
		/// </summary>
		public void Cancel()
		{
			MainThread.Cancel(this);
			Dispose();
		}

		/// <summary>
		/// Releases all resource used by the <see cref="ThreadCallData"/> object.
		/// </summary>
		/// <remarks>Call <see cref="Dispose"/> when you are finished using the <see cref="ThreadCallData"/>. The <see cref="Dispose"/>
		/// method leaves the <see cref="ThreadCallData"/> in an unusable state. After calling <see cref="Dispose"/>, you must
		/// release all references to the <see cref="ThreadCallData"/> so the garbage collector can reclaim the memory that the
		/// <see cref="ThreadCallData"/> was occupying.</remarks>
		public void Dispose()
		{
			Method = null;
			OnDisposed(this);
		}

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents the current <see cref="ThreadCallData"/>.
		/// </summary>
		/// <returns>A <see cref="System.String"/> that represents the current <see cref="ThreadCallData"/>.</returns>
		override public string ToString()
		{
			return string.Format("[ThreadCallData](method=\"{0}\")", MethodName);
		}
	}
}