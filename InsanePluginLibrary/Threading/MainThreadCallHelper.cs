﻿/*
	Copyright 2015 Danilo Nishimura

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

using UnityEngine;

namespace InsanePluginLibrary.Threading
{
	/// <summary>
	/// The Monobehaviour that queues the invocations and dispatches them in the Unity Loop
	/// </summary>
	public class MainThreadCallHelper : MonoBehaviour
	{
		[SerializeField]
		private bool runUpdate = true;

		[SerializeField]
		private bool persistThroughScenes = true;

		private void OnApplicationPause(bool paused)
		{
			runUpdate = !paused;
		}

		private void Awake()
		{
			if (persistThroughScenes)
			{
				Object.DontDestroyOnLoad(this);
				Object.DontDestroyOnLoad(this.gameObject);
			}
		}

		private void Update()
		{
			if (MainThread.initialized && runUpdate)
			{
				int actionCount = MainThread.immediateActions.Count;
				if (actionCount > 0)
				{
					for (int i = 0; i < actionCount; i++)
					{
						MainThread.Invoke(MainThread.immediateActions[i]);
					}

					MainThread.ClearInvokedMethods();
				}
			}
		}
	}
}