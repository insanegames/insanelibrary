﻿namespace InsanePluginLibrary.Log
{
	/// <summary>
	/// The Level of a Log instance.
	/// </summary>
	public enum LogLevel
	{
		/// <summary>
		/// Basic information
		/// </summary>
		INFO,

		/// <summary>
		/// Warnings
		/// </summary>
		WARNING,

		/// <summary>
		/// Errors and Exceptions
		/// </summary>
		ERROR
	}
}