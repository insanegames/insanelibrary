﻿/*
	Copyright 2017 Danilo Nishimura

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// InsaneLog_LOCAL
//#define LOG

using System;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

namespace InsanePluginLibrary.Log
{
	/// <summary>
	/// This class provides a way to Log information and retreive it later through a bunch of useful methods.
	/// When combined with its editor, delivers a better debugging experience, using features such as filtering, Log timestamps, Stack navigation and more.
	/// </summary>
	public class InsaneLog
	{
		/// <summary>
		/// This event is dispatched whenever a Log action is taken.
		/// </summary>
		static public event Action<InsaneLogData> OnLog = delegate { };

		/// <summary>
		/// Will the Stack Trace be performed at the logging method?
		/// </summary>
		static public bool LogStackTrace = true;

		/// <summary>
		/// The Lock object
		/// </summary>
		static private UnityEngine.Object insaneLock = new UnityEngine.Object();

		/// <summary>
		/// The list of logged information.
		/// </summary>
		static private List<InsaneLogData> dataList;

		/// <summary>
		/// The list containing all current log information
		/// </summary>
		static public List<InsaneLogData> DataList
		{
			get
			{
				return dataList ?? (dataList = new List<InsaneLogData>());
			}
		}

		#region Logging Methods

		/// <summary>
		/// Writes a Log into the Log Window
		/// </summary>
		/// <param name="info">The information to be logged</param>
		/// <param name="group">Optional group parameter for fast filtering</param>
		static public void Log(object info, string group = null)
		{
			PerformLog(info, UnityEngine.Debug.Log, group);
		}

		/// <summary>
		/// Writes a formatted Log into the Log Window
		/// </summary>
		/// <param name="format">The formatted string</param>
		/// <param name="info">The information array which will be replaced at the format string</param>
		static public void LogFormat(string format, params object[] info)
		{
			PerformLog(string.Format(format, info), UnityEngine.Debug.Log, null);
		}

		/// <summary>
		/// Writes a formatted Log into the Log Window
		/// </summary>
		/// <param name="group">Optional group parameter for fast filtering</param>
		/// <param name="format">The formatted string</param>
		/// <param name="info">The information array which will be replaced at the format string</param>
		static public void LogFormatGroup(string group, string format, params object[] info)
		{
			PerformLog(string.Format(format, info), UnityEngine.Debug.Log, group);
		}

		/// <summary>
		/// Writes a Warning Log into the Log Window
		/// </summary>
		/// <param name="info">The information to be logged</param>
		/// <param name="group">Optional group parameter for fast filtering</param>
		static public void LogWarning(object info, string group = null)
		{
			PerformLog(info, UnityEngine.Debug.LogWarning, group, LogLevel.WARNING);
		}

		/// <summary>
		/// Writes a formatted Warning Log into the Log Window
		/// </summary>
		/// <param name="format">The formatted string</param>
		/// <param name="info">The information array which will be replaced at the format string</param>
		static public void LogWarningFormat(string format, params object[] info)
		{
			PerformLog(string.Format(format, info), UnityEngine.Debug.LogWarning, null, LogLevel.WARNING);
		}

		/// <summary>
		/// Writes a formatted Warning Log into the Log Window
		/// </summary>
		/// <param name="group">Optional group parameter for fast filtering</param>
		/// <param name="format">The formatted string</param>
		/// <param name="info">The information array which will be replaced at the format string</param>
		static public void LogWarningFormatGroup(string group, string format, params object[] info)
		{
			PerformLog(string.Format(format, info), UnityEngine.Debug.LogWarning, group, LogLevel.WARNING);
		}

		/// <summary>
		/// Writes a Error Log into the Log Window
		/// </summary>
		/// <param name="info">The information to be logged</param>
		/// <param name="group">Optional group parameter for fast filtering</param>
		static public void LogError(object info, string group = null)
		{
			PerformLog(info, UnityEngine.Debug.LogError, group, LogLevel.ERROR);
		}

		/// <summary>
		/// Writes a formatted Error Log into the Log Window
		/// </summary>
		/// <param name="format">The formatted string</param>
		/// <param name="info">The information array which will be replaced at the format string</param>
		static public void LogErrorFormat(string format, params object[] info)
		{
			PerformLog(string.Format(format, info), UnityEngine.Debug.LogError, null, LogLevel.ERROR);
		}

		/// <summary>
		/// Writes a formatted Error Log into the Log Window
		/// </summary>
		/// <param name="group">Optional group parameter for fast filtering</param>
		/// <param name="format">The formatted string</param>
		/// <param name="info">The information array which will be replaced at the format string</param>
		static public void LogErrorFormatGroup(string group, string format, params object[] info)
		{
			PerformLog(string.Format(format, info), UnityEngine.Debug.LogError, group, LogLevel.ERROR);
		}

		/// <summary>
		/// Writes a Exception Log into the Log Window
		/// </summary>
		/// <param name="info">The information to be logged</param>
		/// <param name="group">Optional group parameter for fast filtering</param>
		static public void LogException(object info, string group = null)
		{
			PerformLog(info, UnityEngine.Debug.LogError, group, LogLevel.ERROR);
		}

		/// <summary>
		/// Writes a formatted Exception Log into the Log Window
		/// </summary>
		/// <param name="format">The formatted string</param>
		/// <param name="info">The information array which will be replaced at the format string</param>
		static public void LogExceptionFormat(string format, params object[] info)
		{
			PerformLog(string.Format(format, info), UnityEngine.Debug.LogError, null, LogLevel.ERROR);
		}

		/// <summary>
		/// Writes a formatted Exception Log into the Log Window
		/// </summary>
		/// <param name="group">Optional group parameter for fast filtering</param>
		/// <param name="format">The formatted string</param>
		/// <param name="info">The information array which will be replaced at the format string</param>
		static public void LogExceptionFormatGroup(string group, string format, params object[] info)
		{
			PerformLog(string.Format(format, info), UnityEngine.Debug.LogError, group, LogLevel.ERROR);
		}

		#endregion Logging Methods

		/// <summary>
		/// Creates a <see cref="InsaneLogData"/> object, filling it with log information.
		/// </summary>
		/// <param name="info">The message to be logged. It gets converted to <see cref="string"/>. <see cref="null"/> information is written as \"null\" string.</param>
		/// <param name="logMethod">The internal log method, used to dispatch different levels of logging and string format overloads.</param>
		/// <param name="group">The log group in which this message should be logged into.</param>
		/// <param name="level">The information level of the message.</param>
		static private void PerformLog(object info, System.Action<object> logMethod, string group = null, LogLevel level = LogLevel.INFO)
		{
#if DEBUG
			try
			{
#endif
				lock (insaneLock)
				{
					string log = (info == null) ? "null" : info.ToString();

					InsaneLogData logData = new InsaneLogData();
					logData.Text = log;

					if (Threading.MainThread.IsOnMainThread())
					{
						LogStackTrace = PlayerPrefs.GetInt("InsaneLog_StackTrace", 1) == 1;
					}

					if (LogStackTrace)
					{
						logData.StackTrace = new StackTrace(true);
					}

					logData.Group = group;
					logData.Time = DateTime.Now;

					logData.level = level;

					DataList.Add(logData);

					OnLog(logData);
				}
#if DEBUG
			}
			catch (System.Exception exp)
			{
				UnityEngine.Debug.LogException(exp);
			}
#endif
		}
	}
}