﻿/*
	Copyright 2017 Danilo Nishimura

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

using System;

namespace InsanePluginLibrary
{
	/// <summary>
	/// This class can be used as means of keeping track of all singleton instances for later cleanup.
	/// </summary>
	public static class Singleton
	{
		static private object key = new object();

		/// <summary>
		/// Sets up a Singleton reference to the provided Type.
		/// </summary>
		/// <typeparam name="T">The Type to be registred.</typeparam>
		/// <param name="instance">The Type instance.</param>
		public static void Set<T>(T instance)
		{
			Binding.Set<T>(key, instance);
		}

		/// <summary>
		/// Sets up a Singleton reference to the provided Type.
		/// </summary>
		/// <param name="instance">The Type instance.</param>
		/// <param name="type">Type of the instance.</param>
		public static void Set(object instance, Type type)
		{
			Binding.Set(key, instance, type);
		}

		/// <summary>
		/// Remove a instance from the Singleton list.
		/// </summary>
		/// <typeparam name="T">The Type to be removed.</typeparam>
		/// <param name="instance">The instance bound.</param>
		/// <returns>True if the instance was bound.</returns>
		public static bool TryClear<T>(T instance)
		{
			return Binding.TryClearType<T>(key, instance);
		}

		/// <summary>
		/// Remove a instance from the Singleton list.
		/// </summary>
		/// <typeparam name="T">The Type to be removed.</typeparam>
		public static void Clear<T>()
		{
			Binding.ClearType<T>(key);
		}

		/// <summary>
		/// Remove a instance from the Singleton list.
		/// </summary>
		/// <param name="type">The Type to be removed.</param>
		public static void Clear(Type type)
		{
			Binding.ClearType(key, type);
		}

		/// <summary>
		/// Removes all Singleton references.
		/// </summary>
		public static void ClearAll()
		{
			Binding.ClearObject(key);
		}

		/// <summary>
		/// Returns true if the provided Type is registred as Singleton instance.
		/// </summary>
		/// <typeparam name="T">The Type to be checked.</typeparam>
		/// <returns>True if the Type is registred.</returns>
		public static bool Contains<T>()
		{
			return Binding.Contains<T>(key);
		}

		/// <summary>
		/// Returns true if the provided Type is registred as Singleton instance.
		/// </summary>
		/// <param name="type">The Type to be checked.</param>
		/// <returns>True if the Type is registred.</returns>
		public static bool Contains(Type type)
		{
			return Binding.Contains(key, type);
		}

		/// <summary>
		/// Checks if a Singleton instance is registred and doesn't point to null.
		/// </summary>
		/// <typeparam name="T">The Type to be checked.</typeparam>
		/// <returns>True if the Singleton instance is registred and doesn't point to null.</returns>
		public static bool Exists<T>()
		{
			return Binding.Exists<T>(key);
		}

		/// <summary>
		/// Checks if a Singleton instance is registred and doesn't point to null.
		/// </summary>
		/// <param name="type">The Type to be checked.</param>
		/// <returns>True if the Singleton instance is registred and doesn't point to null.</returns>
		public static bool Exists(Type type)
		{
			return Binding.Exists(key, type);
		}

		/// <summary>
		/// Gets the registred instance of Type.
		/// </summary>
		/// <typeparam name="T">The Type of the instance.</typeparam>
		/// <returns>The object of Type T.</returns>
		public static T Get<T>()
		{
			return Binding.Get<T>(key);
		}

		/// <summary>
		/// Gets the registred instance of Type.
		/// </summary>
		/// <param name="type">The Type of the instance.</param>
		/// <returns>The object of Type T.</returns>
		public static object Get(Type type)
		{
			return Binding.Get(key, type);
		}

		/// <summary>
		/// Attempts to get a instance of the provided Type.
		/// </summary>
		/// <typeparam name="T">The Type of the instance.</typeparam>
		/// <param name="instance">The instance of the Type, if found.</param>
		/// <returns>True if a instance is found.</returns>
		public static bool TryGet<T>(out T instance)
		{
			return Binding.TryGet<T>(key, out instance);
		}

		/// <summary>
		/// Attempts to get a instance of the provided Type.
		/// </summary>
		/// <param name="type">The Type of the instance.</param>
		/// <param name="instance">The instance of the Type, if found.</param>
		/// <returns>True if a instance is found.</returns>
		public static bool TryGet(out object instance, Type type)
		{
			return Binding.TryGet(key, out instance, type);
		}
	}
}