﻿using System;
using System.Collections.Generic;

namespace InsanePluginLibrary
{
	/// <summary>
	/// This class creates a binding between a object and many type instances.
	/// It's statically accessible, and allows you to quickly access objects without going through complex chains of hierarchical objects.
	/// </summary>
	public static class Binding
	{
		/// <summary>
		/// This is essencially the list of bindings.
		/// Binds a <see cref="object"/> to a <see cref="Dictionary{Type, object}"/> which binds <see cref="Type"/>s to <see cref="object"/>s.
		/// </summary>
		private static Dictionary<object, Dictionary<Type, object>> entityLink = new Dictionary<object, Dictionary<Type, object>>();

		/// <summary>
		/// Returns True if any instance is bound to the provided key object. If you intent to use the value, consider using Binding.TryGet()
		/// </summary>
		/// <param name="key">The object used as key</param>
		/// <returns>True if any instance is bound to the provided key object.</returns>
		public static bool Any(object key)
		{
			return entityLink.ContainsKey(key);
		}

		/// <summary>
		/// Returns a new list containing all <see cref="Type"/>s bound to the key object.
		/// </summary>
		/// <param name="key">The object used as key.</param>
		/// <returns>A list of <see cref="Type"/>s registred.</returns>
		public static List<Type> GetBindingKeys(object key)
		{
			return new List<Type>(entityLink[key].Keys);
		}

		/// <summary>
		/// Returns a new ist containing all <see cref="object"/>s bound to the key object
		/// </summary>
		/// <param name="key">The object used as key.</param>
		/// <returns>A list of <see cref="object"/>s registred.</returns>
		public static List<object> GetBindingValues(object key)
		{
			return new List<object>(entityLink[key].Values);
		}

		/// <summary>
		/// Binds the key to the object, regardless of internal initialization or valid references.
		/// Pros: Faster, no allocation. Cons: No checks, error prone. When in doubt, use the <see cref="Set{T}(object, T)"/> method.
		/// </summary>
		/// <typeparam name="T">The <see cref="Type"/> of the object to be bound. This can be omitted.</typeparam>
		/// <param name="key">The object to be used as key.</param>
		/// <param name="instance">The instance of the provided <see cref="Type"/>.</param>
		public static void SetUnchecked<T>(object key, T instance)
		{
			entityLink[key][typeof(T)] = instance;
		}

		/// <summary>
		/// Binds the key to the object, regardless of internal initialization or valid references.
		/// Pros: Faster, no allocation. Cons: No checks, error prone. When in doubt, use the <see cref="Set(object, object, Type)"/> method.
		/// </summary>
		/// <param name="key">The object to be used as key.</param>
		/// <param name="instance">The instance of the provided <see cref="Type"/>.</param>
		/// <param name="type">The <see cref="Type"/> of the object to be bound.</param>
		public static void SetUnchecked(object key, object instance, Type type)
		{
			entityLink[key][type] = instance;
		}

		/// <summary>
		/// Binds the key to the object. If the object doesn't have any bindings, create a new one.
		/// </summary>
		/// <typeparam name="T">The <see cref="Type"/> of the object to be bound. If omitted, the object's type will be used.</typeparam>
		/// <param name="key">The object to be used as key.</param>
		/// <param name="instance">The instance of the provided <see cref="Type"/>.</param>
		public static void Set<T>(object key, T instance)
		{
			Type type = typeof(T);
			if (entityLink.ContainsKey(key))
			{
				SetUnchecked(key, instance);
			}
			else
			{
				var dict = new Dictionary<Type, object>();
				dict.Add(type, instance);
				entityLink.Add(key, dict);
			}
		}

		/// <summary>
		/// Binds the key to the object. If the object doesn't have any bindings, create a new one.
		/// </summary>
		/// <param name="key">The object to be used as key.</param>
		/// <param name="instance">The instance of the provided <see cref="Type"/>.</param>
		/// <param name="type">The <see cref="Type"/> of the object to be bound.</param>
		public static void Set(object key, object instance, Type type)
		{
			if (entityLink.ContainsKey(key))
			{
				SetUnchecked(key, instance, type);
			}
			else
			{
				var dict = new Dictionary<Type, object>();
				dict.Add(type, instance);
				entityLink.Add(key, dict);
			}
		}

		/// <summary>
		/// Gets the <see cref="T"/> bound to the provided object, regardless of internal initialization or valid references.
		/// Pros: Faster, no allocation. Cons: No checks, error prone. When in doubt, use the <see cref="Get{T}(object, T)"/> method.
		/// </summary>
		/// <typeparam name="T">The <see cref="Type"/> of the instance bound to the provided object. This can be omitted.</typeparam>
		/// <param name="key">The object to be used as key.</param>
		/// <returns>The instance for the provided <see cref="Type"/></returns>
		public static T GetUnchecked<T>(object key)
		{
			return (T)entityLink[key][typeof(T)];
		}

		/// <summary>
		/// Gets the <see cref="T"/> bound to the provided object, regardless of internal initialization or valid references.
		/// Pros: Faster, no allocation. Cons: No checks, error prone. When in doubt, use the <see cref="Get{T}(object, T)"/> method.
		/// </summary>
		/// <param name="key">The object to be used as key.</param>
		/// <param name="type">The <see cref="Type"/> of the instance bound to the provided object.</param>
		/// <returns>The instance for the provided <see cref="Type"/></returns>
		public static object GetUnchecked(object key, Type type)
		{
			return entityLink[key][type];
		}

		/// <summary>
		/// Gets the <see cref="T"/> bound to the provided object. If no match, a <see cref="Exception"/> is thrown.
		/// See Also: <see cref="TryGet{T}(object, out T)"/>
		/// </summary>
		/// <typeparam name="T">The <see cref="Type"/> of the instance bound to the provided object. This can be omitted.</typeparam>
		/// <param name="key">The object to be used as key.</param>
		/// <returns>The instance for the provided <see cref="Type"/></returns>
		public static T Get<T>(object key)
		{
			if (entityLink.ContainsKey(key))
			{
				return GetUnchecked<T>(key);
			}
			else
			{
				throw new Exception("Binding not set.");
			}
		}

		/// <summary>
		/// Gets the object bound to the provided key object. If no match, a <see cref="Exception"/> is thrown.
		/// This is a alternative for the <see cref="Get{T}(object)"/>  method.
		/// See Also: <see cref="TryGet{T}(object, out T)"/>
		/// </summary>
		/// <param name="key">The object to be used as key.</param>
		/// <param name="type">The <see cref="Type"/> of the instance bound to the provided object.</param>
		/// <returns>The instance for the provided <see cref="Type"/></returns>
		public static object Get(object key, Type type)
		{
			if (entityLink.ContainsKey(key))
			{
				return GetUnchecked(key, type);
			}
			else
			{
				throw new Exception(string.Format("Binding Exception: Key \"{0}\" not set.\nMake sure to bind the key before retrieving the value. See Binding.Set().", key.ToString()));
			}
		}

		/// <summary>
		/// Checks if a <see cref="T"/> is bound to the object.
		/// </summary>
		/// <typeparam name="T">The <see cref="Type"/> of the instance bound to the provided object.</typeparam>
		/// <param name="key">The object to be used as key.</param>
		/// <returns>True if the <see cref="Type"/> is bound to the key object.</returns>
		public static bool Contains<T>(object key)
		{
			if (entityLink.ContainsKey(key))
			{
				return entityLink[key].ContainsKey(typeof(T));
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// Checks if a <see cref="Type"/> is bound to the object.
		/// </summary>
		/// <param name="key">The object to be used as key.</param>
		/// <param name="type">The <see cref="Type"/> of the instance bound to the provided object.</param>
		/// <returns>True if the <see cref="Type"/> is bound to the key object.</returns>
		public static bool Contains(object key, Type type)
		{
			if (entityLink.ContainsKey(key))
			{
				return entityLink[key].ContainsKey(type);
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// Checks if a <see cref="T"/> is bound to the object and is not null.
		/// </summary>
		/// <typeparam name="T">The <see cref="Type"/> of the instance bound to the provided object.</typeparam>
		/// <param name="key">The object to be used as key.</param>
		/// <returns>True if the key object has a binding to the <see cref="Type"/> and it's not null.</returns>
		public static bool Exists<T>(object key)
		{
			if (Contains<T>(key))
			{
				return entityLink[key][typeof(T)] != null;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// Checks if a <see cref="Type"/> is bound to the object and is not null.
		/// </summary>
		/// <param name="key">The object to be used as key.</param>
		/// <param name="type">The <see cref="Type"/> of the instance bound to the provided object.</param>
		/// <returns>True if the key object has a binding to the <see cref="Type"/> and it's not null.</returns>
		public static bool Exists(object key, Type type)
		{
			if (Contains(key, type))
			{
				return entityLink[key][type] != null;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// Remove reference to the provided <see cref="T"/>.
		/// </summary>
		/// <typeparam name="T">The <see cref="Type"/> of the instance bound to the provided object.</typeparam>
		/// <param name="key">The object to be used as key for the instance removal.</param>
		/// <param name="instance">The instance bound.</param>
		/// <returns>True if the instance was bound.</returns>
		public static bool TryClearType<T>(object key, T instance)
		{
			T boundInstance;
			
			if (TryGet(key, out boundInstance) && EqualityComparer<T>.Default.Equals(boundInstance, instance))
			{
				entityLink[key].Remove(typeof(T));
				return true;
			}

			return false;
		}

		/// <summary>
		/// Remove reference to the provided <see cref="T"/>.
		/// </summary>
		/// <typeparam name="T">The <see cref="Type"/> of the instance bound to the provided object.</typeparam>
		/// <param name="key">The object to be used as key for the instance removal.</param>
		public static void ClearType<T>(object key)
		{
			if (Contains<T>(key))
			{
				entityLink[key].Remove(typeof(T));
			}
		}

		/// <summary>
		/// Remove reference to the provided <see cref="Type"/>.
		/// </summary>
		/// <param name="key">The object to be used as key for the instance removal.</param>
		/// <param name="type">The <see cref="Type"/> of the instance bound to the provided object.</param>
		public static void ClearType(object key, Type type)
		{
			if (Contains(key, type))
			{
				entityLink[key].Remove(type);
			}
		}

		/// <summary>
		/// Removes all instance references bound to the provided key object.
		/// </summary>
		/// <param name="key">The object to have the bound instances removed.</param>
		public static void ClearObject(object key)
		{
			entityLink[key].Clear();
		}

		/// <summary>
		/// Removes all instance references bound to the provided key object.
		/// </summary>
		/// <param name="key">The object to have the bound instances removed.</param>
		/// <returns>True if the reference is bound and exists.</returns>
		public static bool TryClearObject(object key)
		{
			Dictionary<Type, Object> dict;

			if (entityLink.TryGetValue(key, out dict))
			{
				dict.Clear();
				entityLink.Remove(key);

				return true;
			}

			return false;
		}

		/// <summary>
		/// Removes all instance references from all objects.
		/// </summary>
		public static void ClearAll()
		{
			foreach (var dict in entityLink.Values)
			{
				dict.Clear();
			}
			entityLink.Clear();
		}

		/// <summary>
		/// Attempts to get the instance bound to the key object.
		/// </summary>
		/// <typeparam name="T">The <see cref="Type"/> of the instance bound to the provided object.</typeparam>
		/// <param name="key">The object to be checked.</param>
		/// <param name="instance">If any instance is found, it gets output here.</param>
		/// <returns>True if the reference is bound and exists.</returns>
		public static bool TryGet<T>(object key, out T instance)
		{
			var exists = Exists<T>(key);
			instance = exists ? Get<T>(key) : default(T);
			return exists;
		}

		/// <summary>
		/// Attempts to get the instance bound to the key object.
		/// </summary>
		/// <param name="key">The object to be checked.</param>
		/// <param name="instance">If any instance is found, it gets output here. If not, null.</param>
		/// <param name="type">The <see cref="Type"/> of the instance bound to the provided object.</param>
		/// <returns>True if the reference is bound and exists.</returns>
		public static bool TryGet(object key, out object instance, Type type)
		{
			var exists = Exists(key, type);
			instance = exists ? Get(key, type) : null;
			return exists;
		}
	}
}