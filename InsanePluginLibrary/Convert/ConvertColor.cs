﻿/*
	Copyright 2015 Danilo Nishimura

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

using UnityEngine;

namespace InsanePluginLibrary.Convert
{
	/// <summary>
	/// This struct holds hue, saturation and luminance values.
	/// </summary>
	public struct HSL
	{
		/// <summary>
		/// The Hue value
		/// </summary>
		public float h;

		/// <summary>
		/// The Saturation value
		/// </summary>
		public float s;

		/// <summary>
		/// The Luminance value
		/// </summary>
		public float l;

		/// <summary>
		/// The Alpha value
		/// </summary>
		public float a;

		/// <summary>
		/// Creates a new Hue-Saturation-Luminance object
		/// </summary>
		/// <param name="h">The Hue value</param>
		/// <param name="s">The Saturation value</param>
		/// <param name="l">The Luminance value</param>
		/// <param name="a">The Alpha value</param>
		public HSL(float h, float s, float l, float a = 1)
		{
			this.h = h;
			this.s = s;
			this.l = l;
			this.a = a;
		}

		/// <summary>
		/// Returns a string representation of a HSLA object.
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return string.Format("HSLA({0}, {1}, {2}, {3})", h, s, l, a);
		}
	}

	/// <summary>
	/// Main class for color conversion
	/// </summary>
	public class ConvertColor
	{
		/// <summary>
		/// Converts a HSL Color to RGBA Color
		/// </summary>
		/// <param name="hsl">The HSL value</param>
		/// <returns>The converted color</returns>
		static public Color ToColor(HSL hsl)
		{
			if (hsl.s == 0)
			{
				return new Color(hsl.l, hsl.l, hsl.l, hsl.a);
			}
			else if (hsl.l == 0)
			{
				return new Color(0, 0, 0, hsl.a);
			}

			hsl.h /= 60;
			int i = Mathf.FloorToInt(hsl.h);
			float f = hsl.h - i;

			float p = hsl.l * (1 - hsl.s);
			float q = hsl.l * (1 - hsl.s * f);
			float t = hsl.l * (1 - hsl.s * (1 - f));

			switch (i)
			{
				case 0: return new Color(hsl.l, t, p, hsl.a);
				case 1: return new Color(q, hsl.l, p, hsl.a);
				case 2: return new Color(p, hsl.l, t, hsl.a);
				case 3: return new Color(p, q, hsl.l, hsl.a);
				case 4: return new Color(t, p, hsl.l, hsl.a);
				default: return new Color(hsl.l, p, q, hsl.a);
			}
		}

		/// <summary>
		/// Converts a valid string (RRGGBBAA, no sharp nor 0x notation) to RGB color.
		/// </summary>
		/// <param name="hex">The valid HEX string</param>
		/// <returns>The correspondent RGBA Color value</returns>
		static public Color ToColor(string hex)
		{
			int _hex = int.Parse(hex, System.Globalization.NumberStyles.HexNumber);
			return new Color(
				((_hex & 0xff000000) >> 24) / 255f,
				((_hex & 0xff0000) >> 16) / 255f,
				((_hex & 0xff00) >> 8) / 255f,
				((_hex & 0xff)) / 255f);
		}

		/// <summary>
		/// Converts RGB Color to HSL Color
		/// </summary>
		/// <param name="color">The color to be converted</param>
		/// <returns>The converted HSL value</returns>
		static public HSL ToHSL(Color color)
		{
			HSL _hsl = new HSL(0, 0, 0, color.a);

			float min;
			float max;

			min = Mathf.Min(color.r, color.g, color.b);
			max = Mathf.Max(color.r, color.g, color.b);
			_hsl.l = max;

			float diff = max - min;
			if (max != 0 && diff != 0)
			{
				_hsl.s = diff / max;
			}
			else
			{
				_hsl.s = 0;
				_hsl.h = -1;
				return _hsl;
			}

			if (color.r == max)
			{
				_hsl.h = ((color.g - color.b) / diff) * 60;
			}
			else if (color.g == max)
			{
				_hsl.h = (2 + (color.b - color.r) / diff) * 60;
			}
			else
			{
				_hsl.h = (4 + (color.r - color.g) / diff) * 60;
			}

			if (_hsl.h < 0)
			{
				_hsl.h += 360;
			}

			return _hsl;
		}

		/// <summary>
		/// Converts Hex to HSL using the Color conversion as middleman.
		/// </summary>
		/// <param name="hex">The Hexadecimal color value (RRGGBBAA)</param>
		/// <returns>The Hue Saturation Brightness value</returns>
		static public HSL ToHSL(string hex)
		{
			return ToHSL(ToColor(hex));
		}

		/// <summary>
		/// Converts RGBA Color to Hexadecimal format (RRGGBBAA, no sharp nor 0x notation).
		/// </summary>
		/// <param name="color">The color to be converted</param>
		/// <returns>The hexadecimal formatted string</returns>
		public static string ToHEX(Color color)
		{
			return ((int)((byte)(color.r * 255) << 24) | ((byte)(color.g * 255) << 16) | ((byte)(color.b * 255) << 8) | ((byte)(color.a * 255) << 0)).ToString("X8");
		}

		/// <summary>
		/// Converts HSL to Hexadecimal format (RRGGBBAA, no sharp nor 0x notation) using the Color conversion as middleman.
		/// </summary>
		/// <param name="hsl">The HSL to be converted</param>
		/// <returns>The hexadecimal formatted string</returns>
		public static string ToHEX(HSL hsl)
		{
			return ToHEX(ToColor(hsl));
		}
	}
}