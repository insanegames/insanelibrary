﻿/*
	Copyright 2015 Danilo Nishimura

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

using System;
using System.Collections.Generic;

namespace InsanePluginLibrary.Environment
{
	/// <summary>
	/// This class facilitates the process of argument retrieval when launching the game as "MY_GAME_NAME.exe --myProp:myValue".
	/// </summary>
	public static class ApplicationArguments
	{
		/// <summary>
		/// Debug String
		/// </summary>
		private const string classText = "[InsaneLibrary::ApplicationArguments] ";

		/// <summary>
		/// The initialization state.
		/// </summary>
		public static bool initialized { get; private set; }

		/// <summary>
		/// The keys and values from the application arguments
		/// </summary>
		public static Dictionary<string, string> KeyValue { get; private set; }

		/// <summary>
		/// Returns a string value for the given key.
		/// </summary>
		/// <param name="key">The argument key.</param>
		/// <returns>The value of the application argument key.</returns>
		static public string GetValue(string key)
		{
			if (KeyValue.ContainsKey(key))
			{
				return KeyValue[key];
			}
			else
			{
				throw new Exception(string.Format("{0} ERROR: No value for key \"{1}\"", classText, key));
			}
		}

		/// <summary>
		/// Converts the argument value to the provided type.
		/// </summary>
		/// <typeparam name="T">The type to be converted to.</typeparam>
		/// <param name="key">The argument key.</param>
		/// <returns>The value of the application argument key, converted to the provided type.</returns>
		static public T GetValueAs<T>(string key)
		{
			if (KeyValue.ContainsKey(key))
			{
				return (T)System.Convert.ChangeType(KeyValue[key], typeof(T));
			}
			else
			{
				throw new Exception(string.Format("{0} ERROR: No value for key \"{1}\"", classText, key));
			}
		}

		/// <summary>
		/// Attempts to get a value from the provided key
		/// </summary>
		/// <typeparam name="T">The value <see cref="Type"/></typeparam>
		/// <param name="key">The <see cref="String"/> key</param>
		/// <param name="value">The value if available.</param>
		/// <returns>True if the value is found.</returns>
		static public bool TryGet<T>(string key, out T value)
		{
			if (KeyValue.ContainsKey(key))
			{
				value = (T)System.Convert.ChangeType(KeyValue[key], typeof(T));
				return true;
			}
			else
			{
				value = default(T);
				return false;
			}
		}

		/// <summary>
		/// Returns true if the provided key is provided as application argument.
		/// </summary>
		/// <param name="key">The key to be read.</param>
		/// <returns>True if the provided key is provided as application argument.</returns>
		static public bool ContainsKey(string key)
		{
			return KeyValue.ContainsKey(key);
		}

		/// <summary>
		/// Initializes the class instance.
		/// </summary>
		/// <param name="readArguments">If True, will read the arguments right away.</param>
		static public void Initialize(bool readArguments = true)
		{
			if (!initialized)
			{
#if DEBUG
				UnityEngine.Debug.Log(classText + "Initializing Application Arguments...");
#endif
				KeyValue = new Dictionary<string, string>();
				initialized = true;
				if (readArguments)
				{
					ReadArguments();
				}
			}
			else
			{
#if DEBUG
				UnityEngine.Debug.Log(classText + "Already Initialized.");
#endif
			}
		}

		/// <summary>
		/// Read the arguments from the application and store their values.
		/// </summary>
		static public void ReadArguments()
		{
#if DEBUG
			UnityEngine.Debug.Log(classText + "Reading Application Arguments...");
#endif

			List<string> args = new List<string>(System.Environment.GetCommandLineArgs());

			if (args != null && args.Count > 1)
			{
				//string fullArg = "Full Command Line Arguments:\n" + string.Join("\n", args.ToArray());
				foreach (string arg in args)
				{
					if (arg.Contains("--"))
					{
						if (arg.Contains(":"))
						{
							//is value
							string key = arg.Substring(arg.IndexOf("--") + 2, arg.IndexOf(":") - 2);
							string value = GetValue(arg, string.Format("--{0}:", key));
							if (KeyValue.ContainsKey(key))
							{
								KeyValue[key] = value;
							}
							else
							{
								KeyValue.Add(key, value);
							}
#if DEBUG
							UnityEngine.Debug.Log(string.Format("{0}{1} = {2}", classText, key, value));
#endif
						}
						else
						{
							//is definition
							string key = arg.Substring(arg.IndexOf("--") + 2);
							if (KeyValue.ContainsKey(key))
							{
								KeyValue[key] = "true";
							}
							else
							{
								KeyValue.Add(key, "true");
							}
#if DEBUG
							UnityEngine.Debug.Log(string.Format("{0}{1} = true", classText, key));
#endif
						}
					}
					else
					{
#if DEBUG
						UnityEngine.Debug.Log(classText + "Command line arguments unknown");
#endif
					}
				}
			}
			else
			{
#if DEBUG
				UnityEngine.Debug.Log(classText + "Command line argumens are empty");
#endif
			}
		}

		/// <summary>
		/// Gets the value for the provided key.
		/// </summary>
		/// <param name="raw">The string to be split.</param>
		/// <param name="key">The key to searched.</param>
		/// <returns></returns>
		private static string GetValue(string raw, string key)
		{
			string substring = raw.Substring(raw.IndexOf(key) + key.Length); //split beggining
			if (substring.Contains("--"))
			{
				substring = substring.Substring(0, substring.IndexOf("--")); //split end
			}
			return substring;
		}
	}
}