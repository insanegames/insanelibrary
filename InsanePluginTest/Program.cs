﻿/*
	Copyright 2015 Danilo Nishimura

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

using InsanePluginLibrary;
using InsanePluginLibrary.Convert;
using InsanePluginLibrary.Environment;
using System;
using UnityEngine;

namespace InsanePluginTest
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			ColorTest colorTest = new ColorTest();
			colorTest.TestColorTransformation();

			TemperatureTest temperatureTest = new TemperatureTest();
			temperatureTest.TestTemperatureConversion();

			DistanceTest distanceTest = new DistanceTest();
			distanceTest.TestDistanceConversion();

			BindingsTest bindingsTest = new BindingsTest();
			bindingsTest.TestBindings();

			AABBTest aabbTest = new AABBTest();
			aabbTest.TestAABB();

			ArgumentTest argumentTest = new ArgumentTest();
			argumentTest.TestArguments();

			Console.ReadLine();
		}
	}

	internal class ArgumentTest
	{
		internal void TestArguments()
		{
			//--a:1 --b:1000 --c:-10 --d:a --e:OMG --f h:True

			Console.WriteLine("\n-- Argument Test Begin\n");

			ApplicationArguments.Initialize();
			ApplicationArguments.ReadArguments();

			string[] suggestedKeys = { "a", "b", "c", "d", "e", "f", "h" };

			foreach (var suggestedkey in suggestedKeys)
			{
				var hasValue = ApplicationArguments.ContainsKey(suggestedkey);
				Console.WriteLine(string.Format("ApplicationArguments.ContainsKey(\"{0}\"): {1}", suggestedkey, hasValue));
				if (hasValue)
				{
					Console.WriteLine(string.Format("value for \"{0}\" is {1}", suggestedkey, ApplicationArguments.GetValue(suggestedkey)));
				}
			}

			Console.WriteLine("\n-- Argument Test End\n");
		}
	}

	internal class DistanceTest
	{
		internal void TestDistanceConversion()
		{
			Console.WriteLine("\n-- Distance Conversion Begin");
			LogTest(0.0f);
			LogTest(10.0f);
			LogTest(25.0f);
			LogTest(25.8f);
			LogTest(37.0f);
			LogTest(100.0f);
			Console.WriteLine("\n-- Distance Conversion End\n\n");
		}

		internal void LogTest(float value)
		{
			Console.WriteLine(string.Format("\n\nTesting: {0}", value));

			Foot foot = value;
			Console.WriteLine(string.Format("Foot: {0}", foot));

			Inch inch = ConvertDistance.ToInch(foot);
			Console.WriteLine(string.Format("Inch: {0}", inch));

			Meter meter = ConvertDistance.ToMeter(inch);
			Console.WriteLine(string.Format("Meter: {0}", meter));

			Mile mile = ConvertDistance.ToMile(meter);
			Console.WriteLine(string.Format("Mile: {0}", mile));

			Yard yard = ConvertDistance.ToYard(mile);
			Console.WriteLine(string.Format("Yard: {0}", yard));

			foot = ConvertDistance.ToFoot(yard);
			Console.WriteLine(string.Format("Foot: {0}", foot));
		}
	}

	internal class TemperatureTest
	{
		internal void TestTemperatureConversion()
		{
			Console.WriteLine("\n-- Temperature Conversion Begin");
			LogTest(0.0f);
			LogTest(10.0f);
			LogTest(25.0f);
			LogTest(25.8f);
			LogTest(37.0f);
			LogTest(100.0f);
			Console.WriteLine("\n-- Temperature Conversion End\n\n");
		}

		internal void LogTest(float value)
		{
			Console.WriteLine(string.Format("\n\nTesting: {0}", value));
			Celsius celsius = value;
			Console.WriteLine(string.Format("Celsius: {0}", celsius));
			Kelvin kelvin = ConvertTemperature.ToKelvin(celsius);
			Console.WriteLine(string.Format("Kelvin: {0}", kelvin));
			Fahrenheit fahrenheit = ConvertTemperature.ToFahrenheit(kelvin);
			Console.WriteLine(string.Format("Fahrenheit: {0}", fahrenheit));
			celsius = ConvertTemperature.ToCelsius(fahrenheit);
			Console.WriteLine(string.Format("Celsius: {0}", celsius));
		}
	}

	internal class ColorTest
	{
		internal void TestColorTransformation()
		{
			Console.WriteLine("\n-- Color Conversion Begin\n");
			LogTest(Color.black);
			LogTest(Color.white);
			LogTest(Color.red);
			LogTest(Color.green);
			LogTest(Color.blue);
			LogTest(new Color(0.5f, 0.5f, 0.5f, 1f));
			LogTest(new Color(0.2f, 0.4f, 0.6f, 0.8f));
			Console.WriteLine("\n-- Color Conversion End\n\n");
		}

		internal void LogTest(Color color)
		{
			Console.WriteLine(string.Format("\n\nTesting: {0}", color.ToString()));
			var colorHex = ConvertColor.ToHEX(color);
			Console.WriteLine(string.Format("HEX = {0}", colorHex));

			var hslColor = ConvertColor.ToHSL(colorHex);
			Console.WriteLine(string.Format("HSL = {0}", hslColor.ToString()));

			var hexColor = ConvertColor.ToColor(hslColor);
			Console.WriteLine(string.Format("RGB = {0}", hexColor.ToString()));
		}
	}

	internal class BindingsTest
	{
		internal void TestBindings()
		{
			Console.WriteLine("\n-- Bindings Link Test Begin\n");

			Console.WriteLine("\n-- Running Assignment tests:");

			//String
			LogGenericTest("OMG", this);
			LogGenericTest("NEW", this);

			//Int
			LogGenericTest(0, this);
			LogGenericTest(65536, this);

			//Bool
			LogGenericTest(false, this);
			LogGenericTest(true, this);

			//Double
			LogGenericTest(Math.PI, this);

			//Object
			LogGenericTest(new object(), this);

			//DateTime
			LogGenericTest(System.DateTime.Now, this);

			//Negative testing
			Console.WriteLine("\n########################################\n Running object change tests:");
			var obj = new object();
			Console.WriteLine("\nBinding new object.");
			Binding.Set(this, obj);
			Console.WriteLine("\nChanging assignment.");
			obj = new object();
			Console.WriteLine("\nMatches? Should be False:\n\t- {0} ", Equals(Binding.Get<object>(this), obj));
			Binding.Set(this, obj);
			Console.WriteLine("\nMatches? Should be True:\n\t- {0} ", Equals(Binding.Get<object>(this), obj));

			var otherObj = Binding.Get<object>(this);

			Console.WriteLine("\n########################################\n Running \"null\" tests:");
			Console.WriteLine("\nBinding null string.");
			Binding.Set<string>(this, null);
			Console.WriteLine("\nContains? Should be True:\n\t- {0} ", Binding.Contains<string>(this));
			Console.WriteLine("\nExists? Should be False:\n\t- {0} ", Binding.Exists<string>(this));
			Console.WriteLine("\nMatches null? Should be True:\n\t- {0} ", Binding.Get<string>(this) == null);

			Console.WriteLine("\n########################################\n Clearing Bindings:");
			Console.WriteLine("\nClearing string Bindings.");
			Binding.ClearType<string>(this);
			Console.WriteLine("Has string binding? Should be False:\n\t- {0} ", Binding.Exists<string>(this));
			Console.WriteLine("\nClearing int Bindings.");
			Binding.ClearType<int>(this);
			Console.WriteLine("Has int binding? Should be False:\n\t- {0} ", Binding.Exists<int>(this));

			Console.WriteLine("\n-- Bindings Link Test End\n\n");
		}

		internal void LogGenericTest<T>(T obj, object reference)
		{
			Console.WriteLine("\nBinding \"{0}\" of type {1}.", obj, typeof(T), reference.ToString());
			Binding.Set<T>(reference, obj);
			Console.WriteLine(string.Format("Matching Result:\n\t- {0}",
				Equals(Binding.Get<T>(reference), obj)
				)

			);
		}
	}

	internal class AABBTest
	{
		internal void TestAABB()
		{
			var aabb = new InsanePluginLibrary.Mesh.AABB();
			aabb.Min = new Vector3();
			aabb.Max = new Vector3();

			Console.WriteLine(aabb.Size);
			Console.WriteLine(aabb.Center);
			Console.WriteLine();

			aabb.Max = new Vector3(1, 1, 1);
			Console.WriteLine(aabb.Size);
			Console.WriteLine(aabb.Center);
			Console.WriteLine();

			aabb.Max = new Vector3(2, 2, 2);
			Console.WriteLine(aabb.Size);
			Console.WriteLine(aabb.Center);
			Console.WriteLine();

			aabb.Min = new Vector3(1, 1, 1);
			Console.WriteLine(aabb.Size);
			Console.WriteLine(aabb.Center);
			Console.WriteLine();

			aabb.Min = new Vector3(100, 100, 100);
			aabb.Max = new Vector3(200, 200, 200);
			Console.WriteLine(aabb.Size);
			Console.WriteLine(aabb.Center);
			Console.WriteLine();

			aabb.Min = new Vector3(-100, -100, -100);
			aabb.Max = new Vector3(200, 200, 200);
			Console.WriteLine(aabb.Size);
			Console.WriteLine(aabb.Center);
			Console.WriteLine();

			aabb.Min = new Vector3(100, -200, 200);
			aabb.Max = new Vector3(200, 200, 200);
			Console.WriteLine(aabb.Size);
			Console.WriteLine(aabb.Center);
			Console.WriteLine();

		}
	}

}